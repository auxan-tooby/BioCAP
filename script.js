document.getElementById('biodataform').addEventListener('submit', function(e) {
    e.preventDefault();
    const form = e.target;
    const loadingScreen = document.getElementById('loadingScreen');

    // Show loading screen
    loadingScreen.style.display = 'flex';

    const formData = new FormData(form);
    const formProps = Object.fromEntries(formData);

    fetch('https://script.google.com/macros/s/AKfycbxp_wh30WAz0aKtdE5GvJa5ZnLKnDX4eSrb8CyT96try7j9wr0v49NwKj5R1Y81CPEfyQ/exec', { // Replace with your web app URL
        method: 'POST',
        mode: 'no-cors', // This mode might prevent the response from being read
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(formProps)
    }).then(response => {
        // Hide loading screen
        loadingScreen.style.display = 'none';
        // Show success message
        const successScreen = document.getElementById('successScreen');
        successScreen.style.display = 'flex'; // Show the success screen
        // Clear the form
        form.reset();
    }).catch(error => {
        loadingScreen.style.display = 'none';
        const errorMessage = document.getElementById('errorMessage')
        errorMessage.style.display = 'block';
        setTimeout(() => {
            errorMessage.style.display = 'none';
        }, 5000)
        form.reset();
    });
});